<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Passenger;
use AppBundle\Entity\Trip;
use AppBundle\Form\CustomerType;
use AppBundle\Form\PassengerType;
use AppBundle\Form\TripType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/admin")
 */
class AdminPanelController extends Controller
{
    /**
     * @Route("/", name="admin_panel", methods="GET|POST")
     */
    public function index()
    {
        $request = $this->container->get('request');
        /** @var Customer $customer */
        $customer = $this->getUser();

        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_panel');
        }

        return $this->render('admin/index.html.twig', [
            'customer' => $customer,
            'passengers' => $customer->getPassengers(),
            'trips' => $this->getDoctrine()->getManager()->getRepository(Trip::class)->findByCustomer($customer),
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/passenger-new", name="passenger_new", methods="GET|POST")
     */
    public function newPassenger()
    {
        $request = $this->container->get('request');

        $passenger = new Passenger();
        $passenger->setCustomer($this->getUser());

        $form = $this->createForm(PassengerType::class, $passenger);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($passenger);
            $em->flush();

            return $this->redirectToRoute('admin_panel');
        }


        return $this->render('admin/passenger_new.html.twig', [
            'passenger' => $passenger,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/passenger-{id}", name="passenger_delete", methods="DELETE")
     */
    public function deletePassenger($id)
    {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager();

        $passenger = $em->getRepository(Passenger::class)
            ->find($id);

        if (!$passenger) {
            throw $this->createNotFoundException('Passenger does not exist');
        }

        if ($this->isCsrfTokenValid('delete'.$passenger->getId(), $request->request->get('_token'))) {
            $em->remove($passenger);
            $em->flush();
        }

        return $this->redirectToRoute('admin_panel');
    }


    /**
     * @Route("/trip-new", name="trip_new", methods="GET|POST")
     */
    public function newTrip()
    {
        $request = $this->container->get('request');
        /** @var Customer $customer */
        $customer = $this->getUser();

        $trip = new Trip();
        $trip->setCustomer($customer);

        $form = $this->createForm(TripType::class, $trip, [
            'passengers' => $customer->getPassengers()->toArray()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->flush();

            return $this->redirectToRoute('admin_panel');
        }


        return $this->render('admin/trip_new.html.twig', [
            'trip' => $trip,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/trip-{id}", name="trip_delete", methods="DELETE")
     */
    public function deleteTrip($id)
    {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager();

        $trip = $em->getRepository(Trip::class)
            ->find($id);

        if (!$trip) {
            throw $this->createNotFoundException('Trip does not exist');
        }

        if ($this->isCsrfTokenValid('delete'.$trip->getId(), $request->request->get('_token'))) {
            $em->remove($trip);
            $em->flush();
        }

        return $this->redirectToRoute('admin_panel');
    }

}
