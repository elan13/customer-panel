<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trip
 *
 * @ORM\Table(name="trip")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TripRepository")
 */
class Trip
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="trips")
     */
    private $customer;

    /**
     * @ORM\ManyToMany(targetEntity="Passenger", cascade={"persist"})
     */
    private $passengers;


    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="3", min="2")
     * @ORM\Column(type="string", length=3)
     */
    private $fromCode;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="3", min="2")
     * @ORM\Column(type="string", length=3)
     */
    private $toCode;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime")
     */
    private $departureAt;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime")
     */
    private $arrivalAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param string $customer
     *
     * @return Trip
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set fromCode
     *
     * @param string $fromCode
     *
     * @return Trip
     */
    public function setFromCode($fromCode)
    {
        $this->fromCode = $fromCode;

        return $this;
    }

    /**
     * Get fromCode
     *
     * @return string
     */
    public function getFromCode()
    {
        return $this->fromCode;
    }

    /**
     * Set toCode
     *
     * @param string $toCode
     *
     * @return Trip
     */
    public function setToCode($toCode)
    {
        $this->toCode = $toCode;

        return $this;
    }

    /**
     * Get toCode
     *
     * @return string
     */
    public function getToCode()
    {
        return $this->toCode;
    }

    /**
     * Set departureAt
     *
     * @param \DateTime $departureAt
     *
     * @return Trip
     */
    public function setDepartureAt($departureAt)
    {
        $this->departureAt = $departureAt;

        return $this;
    }

    /**
     * Get departureAt
     *
     * @return \DateTime
     */
    public function getDepartureAt()
    {
        return $this->departureAt;
    }

    /**
     * Set arrivalAt
     *
     * @param \DateTime $arrivalAt
     *
     * @return Trip
     */
    public function setArrivalAt($arrivalAt)
    {
        $this->arrivalAt = $arrivalAt;

        return $this;
    }

    /**
     * Get arrivalAt
     *
     * @return \DateTime
     */
    public function getArrivalAt()
    {
        return $this->arrivalAt;
    }

    /**
     * @return Passenger[]
     */
    public function getPassengers()
    {
        return $this->passengers;
    }

    /**
     * @param $passengers
     * @return $this
     */
    public function setPassengers($passengers)
    {
        $this->passengers = $passengers;

        return $this;
    }


    /**
     * @param Passenger $passenger
     * @return $this
     */
    public function addPassenger(Passenger $passenger)
    {
        if (!in_array($passenger, $this->passengers)) {
            $this->passengers[] = $passenger;
        }

        return $this;
    }
}

