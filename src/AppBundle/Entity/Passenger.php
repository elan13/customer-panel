<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Passenger
 *
 * @ORM\Table(name="passenger")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PassengerRepository")
 */
class Passenger
{
    const TITLE_TYPES = [
        0 => 'Mr.',
        1 => 'Ms.',
        2 => 'Mrs.',
        3 => 'Miss',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="passengers")
     */
    private $customer;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice(callback="getTitleIds")
     * @ORM\Column(type="smallint", length=1)
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=150)
     */
    private $firstname;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=150)
     */
    private $surname;


    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=100)
     */
    private $passportId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param \stdClass $customer
     *
     * @return Passenger
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \stdClass
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Passenger
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Passenger
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Passenger
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getPassportId()
    {
        return $this->passportId;
    }

    /**
     * @param string $passportId
     */
    public function setPassportId($passportId)
    {
        $this->passportId = $passportId;
    }

    public function __toString()
    {
        return  self::TITLE_TYPES[$this->title] . ' ' . $this->firstname . ' ' . $this->surname;
    }

    public static function getTitleIds()
    {
        return array_keys(self::TITLE_TYPES);
    }
}

