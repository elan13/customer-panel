<?php

namespace AppBundle\Form;

use AppBundle\Entity\Passenger;
use AppBundle\Entity\Trip;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TripType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromCode')
            ->add('toCode')
            ->add('departureAt', null, ['widget' => 'single_text'])
            ->add('arrivalAt', null, ['widget' => 'single_text'])
            ->add('passengers', EntityType::class, [
                'class' => 'AppBundle:Passenger',
                'choices' => $options['passengers'],
                'multiple' => true,
                'required' => false,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trip::class,
            'passengers' =>  []
        ]);
    }
}
