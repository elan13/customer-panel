customer-portal
===============

A Symfony project created on March 14, 2019, 8:37 pm.

Follow instruction to set up the app locally:

1. git clone git@bitbucket.org:elan13/customer-panel.git

1. cd ./customer-panel/

1. composer install

1. Clone `app/config/parameters.yml.dist` to `app/config/parameters.yml` and add your own env variables

        set "encoder_algorithm: plaintext"  (dev environment)
        dev credentials are stored in plain text 

1. if your database is new, create it `php app/console doctrine:database:create` 
 
1. php app/console doctrine:migrations:migrate

1. php app/console doctrine:fixtures:load

1. run `php app/console server:run` and the app is accessible http://127.0.0.1:8000

1. noew you can use dev credentials, login: `admin@test.com`, password: `test`