<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190318151205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE trip_passenger (trip_id INT NOT NULL, passenger_id INT NOT NULL, INDEX IDX_26F0A98AA5BC2E0E (trip_id), INDEX IDX_26F0A98A4502E565 (passenger_id), PRIMARY KEY(trip_id, passenger_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trip_passenger ADD CONSTRAINT FK_26F0A98AA5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip_passenger ADD CONSTRAINT FK_26F0A98A4502E565 FOREIGN KEY (passenger_id) REFERENCES passenger (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip ADD customer_id INT DEFAULT NULL, ADD from_code VARCHAR(3) NOT NULL, ADD to_code VARCHAR(3) NOT NULL, ADD departure_at DATETIME NOT NULL, ADD arrival_at DATETIME NOT NULL, DROP customer, DROP fromCode, DROP toCode, DROP departureAt, DROP arrivalAt');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_7656F53B9395C3F3 ON trip (customer_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE trip_passenger');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B9395C3F3');
        $this->addSql('DROP INDEX IDX_7656F53B9395C3F3 ON trip');
        $this->addSql('ALTER TABLE trip ADD customer VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD fromCode VARCHAR(3) NOT NULL COLLATE utf8_unicode_ci, ADD toCode VARCHAR(3) NOT NULL COLLATE utf8_unicode_ci, ADD departureAt DATETIME NOT NULL, ADD arrivalAt DATETIME NOT NULL, DROP customer_id, DROP from_code, DROP to_code, DROP departure_at, DROP arrival_at');
    }
}
