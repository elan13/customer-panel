<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190318140937 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE passenger ADD customerId INT DEFAULT NULL, DROP customer');
        $this->addSql('ALTER TABLE passenger ADD CONSTRAINT FK_3BEFE8DDF17FD7A5 FOREIGN KEY (customerId) REFERENCES passenger (id)');
        $this->addSql('CREATE INDEX IDX_3BEFE8DDF17FD7A5 ON passenger (customerId)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE passenger DROP FOREIGN KEY FK_3BEFE8DDF17FD7A5');
        $this->addSql('DROP INDEX IDX_3BEFE8DDF17FD7A5 ON passenger');
        $this->addSql('ALTER TABLE passenger ADD customer LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:object)\', DROP customerId');
    }
}
