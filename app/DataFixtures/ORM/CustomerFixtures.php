<?php
/**
 * Created by PhpStorm.
 * User: elan
 * Date: 3/14/19
 * Time: 10:08 PM
 */


use AppBundle\Entity\Customer;
use AppBundle\Entity\Passenger;
use AppBundle\Entity\Trip;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class CustomerFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $customer = new Customer();
        $customer->setEmail('admin@test.com');
        $customer->setPassword('test');

        $this->loadPassengers($manager, $customer);
        $this->loadTrips($manager, $customer);

        $manager->persist($customer);
        $manager->flush($customer);
    }

    public function loadPassengers(ObjectManager $manager, Customer $customer, $quantity = 3)
    {
        for ($i = 0; $i < $quantity; $i++) {
            $passenger = new Passenger();
            $passenger
                ->setCustomer($customer)
                ->setTitle(rand(0, 3))
                ->setFirstname('name'.$i)
                ->setSurname('surname'.$i)
                ->setPassportId('passport'.$i)
            ;

            $customer->addPassenger($passenger);
            $manager->persist($passenger);
        }
    }


    public function loadTrips(ObjectManager $manager, Customer $customer, $quantity = 3)
    {
        for ($i = 0; $i < $quantity; $i++) {
            $trip = new Trip();
            $trip
                ->setCustomer($customer)
                ->setFromCode('TR'.$quantity)
                ->setToCode('DS'.$quantity)
                ->setDepartureAt(new \DateTime('-2 days'))
                ->setArrivalAt(new \DateTime('yesterday'))
                ->setPassengers($customer->getPassengers())
            ;

            $manager->persist($trip);
        }
    }
}